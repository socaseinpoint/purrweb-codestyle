# React-native

# Оглавление

- **1. Файловая структура проекта**

  ```
  app/
    App.tsx
    constants.ts
    assets/
      fonts/
      images/
      lang/
      svg/
    components/
    hooks/
    layouts/
    libs/
    navigations/
    screens/
    services/
    store/
      index.ts
      ducks.ts
      authMiddleware.ts
      auth/
        index.ts
        actions.ts
        authSlice.ts
        selectors.ts
    styles/
      base.ts
      colors.ts
      fonts.ts
      sizes.ts
      vendor.ts
    types/
    api/
      index.ts
      openapi.json
      generated/
  ```

  - [1.1.](#rule-1.1) Используйте `App.tsx` для подключения глобальных `providerов` и общей настроики приложения.
  - 1.2. Используйте `constants.ts` как общий список констант.
  - [1.3.](#rule-1.3) Используйте папку `assets/` для хранения всех медиа-файлов.
  - 1.4. Используйте папку `components/` для хранения [общих компонентов](#glossary-common-components).
  - 1.5. Используйте папку `hooks/` для хранения общих hook'ов.
  - 1.6. Используйте папку `libs/` для хранения файлов сторонних библиотек.
  - [1.7.](#rule-1.7) Используйте папку `navigations/` для хранения файлов стека и уникальных screen'ов.
  - [1.8.](#rule-1.8) Используйте папку `screens/` для хранения общих экранов.
  - 1.9. Используйте папку `services/` для хранения различных helper-функции.
  - [1.10.](#rule-1.10) Используйте папку `store/` для хранения файлов `redux` архитектуры.
  - [1.11.](#rule-1.11) Используйте папку `styles/` для хранения общих стилей.
  - 1.12. Используйте папку `types/` для хранения общих или сгенерированных типов (прим apollo codegen).
  - [1.13.](#rule-1.13) Используйте папку `api/` для хранения файлов api сгенерированных по `openapi-typescript`.

- **2. Компоненты**
  Каждый компонент может состоять из
  ```
  ComponentName/
    __generated__/ (*)
    index.tsx
    ComponentName.tsx
    ComponentName.fragments.ts (*)
    styles.ts
    * - with graphql
  ```

  - `__generated__/` - если используется graphql codegen
  - `index.tsx`
  - `ComponentName.fragments.ts` - если есть graphql, то тут содержатся фрагмент(ы), по которым генерируются типы, для `PropsTypes`
  - `styles.ts` - все стили, констаты стилей содержатся здесь, если не используется `styled-components`

- **3. Redux**
  Внутри `store/` папки редусеров по фичам
  <p align="center">
    <img src="img/redux.png">
  </p>
- **4. Graphql** 
  - [4.1.](#rule-4.1) Для описания запросов испольуется фаил `ScreenName/ScreenName.api.tsx`, чтобы все автогенериуемые типы находились на уровне скрина в `__generated__` и можно было повторно использовать запросы в различных компонентах, если нужно.
  - [4.2.](#rule-4.2) По возможности типизировать компоненты через `graphql fragment`.
  - 4.3. Папку `__generated__` добавить в `.gitignore`

- **5. Библиотеки** 
  - [5.1.](#rule-5.1) Список библиотек.

- **6. Скрипты** 
  ```
    "ts:build": "tsc -p tsconfig.build.json --skipLibCheck --resolveJsonModule",
    "svg:transform": "./node_modules/@svgr/cli/bin/svgr --native $IN > $OUT",
    "schema:download": "apollo schema:download --endpoint=http://localhost:5000/graphql/ graphql-schema.json",
    "codegen:generate": "apollo codegen:generate --localSchemaFile=graphql-schema.json --target=typescript --includes=app/**/*.tsx,.ts --tagName=gql --addTypename --globalTypesFile=app/types/graphql-global-types.ts __generated__ --passthroughCustomScalars --customScalarsPrefix=GraphQL_ --addTypename",
    "codegen:fragment": "node utils/download-fragment-types.js http://localhost:5000/graphql",
    "install:pods": "cd ios && pod install && cd ..",
    "update:pods": "cd ios && pod update && cd ..",
    "clear": "watchman watch-del-all && rm -rf node_modules/ && yarn cache clean && yarn install",
    "clean-project": "npx react-native-clean-project --remove-iOS-build --remove-iOS-pods",
    "clean:android": "cd android && ./gradlew clean && cd ..",
    "bundle:android": "cd android && ./gradlew bundleRelease && cd app/build/outputs/bundle/release && open . && cd ../../../../../",
    "bundle:android:debug-apk": "react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res && react-native run-android"
    "bundle:android:assemble": "cd android && ./gradlew assembleRelease && cd app/build/outputs/apk/ && open . && cd ../../../../",
    "bundle:android:assets": "react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res"
  ```

### <a name="rule-1.1"></a> 1.1. Используйте `App.tsx` для подключения глобальных `providerов` и общей настроики приложения.

```
    <ApolloProvider client={client}>
     <ReduxProvider store={persistedStore.store}>
       <PersistGate loading={null} persistor={persistedStore.persistor}>
         <SafeAreaProvider>
           <StatusBar barStyle="light-content" />
           <Navigator />
         </SafeAreaProvider>
       </PersistGate>
     </ReduxProvider>
   </ApolloProvider>
```

### <a name="rule-1.3"></a> 1.3. Используйте папку `assets/` для хранения всех медиа-файлов.

```
 assets/
     fonts/
     images/
     lang/ - файлы языков для i18n
     svg/
```

<details>
  <summary>images/index.ts</summary>

```javascript
export { default as INITIAL_SCREEN_SPLASH } from "./initial-screen-splash.png";
```

</details>

<details>
  <summary>svg/index.ts</summary>

```javascript
export { default as AppLogo } from "./AppLogo";
```

</details>

<details>
  <summary>lang/en.json</summary>

```json
{
  "en": {
    "translation": {
      "screens": {
        "SignIn": {
          "title": "Log In"
        },
        "LandlordProfile": {
          "rentalDurationMin": "Must be greater than or equal {{ min }} days"
        }
      },
      "ui": {
        "buttons": {
          "Select": "Select"
        }
      },
      "errors": {
        "error": "Error",
        "required": "Required"
      }
    }
  }
}
```

</details>

### <a name="rule-1.6"></a> 1.6. Используйте папку `libs/` для хранения файлов сторонних библиотек.

Например файлы конфигруации, клиенты и тп

- `i18n.ts`
- `AsyncStorageClient.ts`
- `Apollo.ts`

### <a name="rule-1.7"></a> 1.7. Используйте папку `navigations/` для хранения файлов стека и уникальных screen'ов.

Логика хранения скринов внутри навигации - если скрин используется только в одном стеке, то хранится внутри папки стека, если используется где-нибудь еще, то хранится в папке `screens/`, например, экран в виде модалки, который одинаковый, для всех ролей в приложении.

Папка обязательно должна содержать:

- `Navigator.tsx` основной фаил навигации, который подключается в `App.tsx`.</br>
Используется для описания логики выбора текущего навигатора, например для авторизованного или неавторизованного пользователя и тд по ролям, если они есть.

- <details>
  <summary>routes.ts - Список всех роутов приложения</summary>

```javascript
  enum AppRoutes {
    Initial = 'Initial',
    Signin = 'Signin',
  }
```

</details>

- Внутри паки `navigations/` находятся навигаторы, для каждой роли, (прим `GuestNavigator/GuestNavigator.tsx`) в которой содержаться `stak'и` уникальные `sceen'ы`
- Папки стеков именуются постфиксом `Stack`, прим `GuestNavigator/LandlordTabStack/MyChats.tsx`
<p align="center">
  <img src="img/navigations.png">
</p>

### <a name="rule-1.8"></a> 1.8. Используйте папку `screens/` для хранения общих экранов.

Структура файлов экрана

```
ScreenName/
 components/ - компоненты только для этого экрана
 ScreenName.tsx
 styles.ts
 index.ts
```

### <a name="rule-1.10"></a> 1.10. Используйте папку `store/` для хранения файлов `redux` архитектуры.
```
  store/
    index.ts - фаил кофигурации redux
    ducks.ts
    authMiddleware.ts
    auth/
      index.ts - фаил экспорта actions, reducer, selectors
      actions.ts
      authSlice.ts
      selectors.ts
```

  <details>
    <summary>index.ts</summary>

```javascript
import AsyncStorage from '@react-native-async-storage/async-storage';
import { configureStore } from '@reduxjs/toolkit';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';

import { authMiddleware } from './authMiddleware';
import { actions, reducer, selectors } from './ducks';

export { actions, selectors };

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

type State = ReturnType<typeof reducer>;

export const rootReducer = (state: State, action: any) => {
  let nextState = state as State | undefined;

  if (action.type === actions.auth.signOut.fulfilled.type) {
    // How to reset store https://twitter.com/dan_abramov/status/703035591831773184
    // @ts-ignore
    nextState = undefined;
  }

  return reducer(nextState, action);
};

// @ts-ignore
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  devTools: true,
  middleware: [thunk, authMiddleware],
});

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

```

  </details>


  <details>
    <summary>ducks.ts</summary>

```javascript
import { combineReducers } from 'redux';

import * as auth from './auth';

export const reducer = combineReducers({
  auth: auth.reducer,
});

export const actions = {
  auth: auth.actions,
};

export const selectors = {
  auth: auth.selectors,
};

```

  </details>


  <details>
    <summary>authMiddleware.ts ty Sergey Ivanov </summary>

```javascript
import { AuthAdminDto } from 'api/generated';
import { AnyAction, Dispatch } from 'redux';
import { REHYDRATE } from 'redux-persist';
import { http } from 'services/http';

import { actions } from './ducks';

export const authMiddleware = () => (next: Dispatch<AnyAction>) => (
  action: AnyAction,
) => {
  if (action.type === actions.auth.signIn.type) {
    const payload = action.payload as AuthAdminDto;
    payload.token?.accessToken &&
      http.setAuthorizationHeader(payload.token.accessToken);
  }

  if (action.type === REHYDRATE) {
    action.payload?.auth?.token &&
      http.setAuthorizationHeader(action.payload.auth.token);
  }

  if (action.type === actions.auth.signOut.fulfilled.type) {
    http.unsetAuthorizationHeader();
  }

  return next(action);
};

```

  </details>

### <a name="rule-1.11"></a> 1.11. Используйте папку `styles/` для хранения общих стилей.

Структура файлов

```
styles/
   base.ts   - общие стили приложения
   colors.ts - список цветов в формате color0: '#fff'
   fonts.ts  - список шрифтов под все платформы
   sizes.ts  - константы размеров
   vendor.ts - общие стили для настройки сторонних библиотек
```

  <details>
    <summary>colors.ts</summary>

```javascript
  export const colors =  {
    transparent: 'transparent',
    color0: '#fff',
    color1: '#000',
    ....
    color30: '#7245fc',
  }
```

  </details>

  <details>
    <summary>fonts.ts</summary>

```javascript
const textNormal = Platform.select({
  ios: () => {
    return { fontFamily: "SF Pro Display", fontWeight: "400" };
  },
  android: () => {
    return { fontFamily: "SF-Pro-Display-Medium" };
  },
})();

export const fonts = {
  thin: {
    ...textThin,
  },
  normal: {
    ...textNormal,
  },
  bold: {
    ...textBold,
  },
  normalItalic: {
    ...textNormalItalic,
  },
  boldItalic: {
    ...textBoldItalic,
  },
};
```

  </details>

  <details>
    <summary>sizes.ts</summary>

```javascript
import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const screenHeight = width < height ? height : width;
const screenWidth = width < height ? width : height;

export const sizes = {
  screen: {
    height: screenHeight,
    width: screenWidth,
  },
  font: {
    tiny: 10,
    small: 12,
    medium: 14,
    large: 16,
    huge: 20,
  },
};
```

  </details>


### <a name="rule-1.13"></a> 1.13 Используйте папку `api/` для хранения файлов api сгенерированных по `openapi-typescript`.
Для того, чтобы руками не писать API запросы + что они принимают и отдают, можно воспользоваться генератором API по swagger'у.

  <details>
    <summary>index.ts</summary>

```javascript
import Config from 'react-native-config';
import { http } from 'services/http';

import { AdminsApi } from './generated';

export default {
  AdminsApi: new AdminsApi(undefined, Config.API_URL, http.axiosInstance),
};

```

  </details>


Можно использовать вот так

  <details>
    <summary>example.ts</summary>

```javascript
const handleSignIn = useAsyncCallback(
    () => {
      return api.AdminsApi.adminsControllerSignIn({
        email: 'test@gmail.com',
        password: 'test@gmail.com',
      });
    },
    {
      onSuccess: (result) => {
        dispatch(actions.auth.signIn(result.data));
      },
      onError: (error) => {
        console.warn(error.message);
      },
    },
  );

```

  </details>


### <a name="rule-4.1"></a> 4.1. Для описания запросов испольуется фаил `ScreenName/ScreenName.api.tsx`
Например

  <details>
    <summary>ScreenName.api.tsx</summary>

```javascript
import { gql } from 'apollo-boost';
import { PlaceDetailsFragment } from './PlaceDetails.fragments';

const GET_PLACE_DETAILS = gql`
  query GetPlaceDetails($placeId: ID!) {
    place(input: { placeId: $placeId }) {
      ...PlaceDetailsFragment
    }
  }
  ${PlaceDetailsFragment}
`;

const CREATE_CHAT_IN_TENANT = gql`
  mutation CreateChatInTenant($userId: ID!) {
    createChat(input: { userId: $userId }) {
      recordId
      record {
        id
      }
    }
  }
`;

const PlaceDetailsApi = {
  queries: {
    GET_PLACE_DETAILS,
  },
  mutations: {
    CREATE_CHAT_IN_TENANT,
  },
};

export default PlaceDetailsApi;

```

  </details>

  ### <a name="rule-4.2"></a> 4.2. По возможности типизировать компоненты через `graphql fragment`.
  Если придерживаться этого правила, то легче будет поддерживать проект при изменении схемы на бекенде.
  </br>

  <details>
    <summary>TextMessage.tsx</summary>

```javascript
import React, { FC } from 'react';
import { Text } from 'react-native';
import styles from './styles';
import { TextMessageFragment } from './__generated__/TextMessageFragment';

const TextMessage: FC<TextMessageFragment> = ({ text }) => {
  return <Text style={styles.text}>{text}</Text>;
};

export default TextMessage;
```

  </details>

  <details>
    <summary>ImageMessage.fragments.tsx</summary>

```javascript
import { gql } from 'apollo-boost';

export const ImageMessageFragment = gql`
  fragment ImageMessageFragment on ImageMessage {
    kind
    imageUrl
  }
`;
```

  </details>

### <a name="rule-5.1"></a> 5.1. Список библиотек.

  - redux-toolkit
  - react-navigation
  - apollo-boost
  - react-final-form
  - react-i18next
  - react-native-svg
  - react-native-config
  - @openapitools/openapi-generator-cli
  - react-async-hook
  - ~~typesafe-actions~~

### Общее

- Модалки создаем через скрины??

## Глоссарий

- <a name="glossary-common-components">Общие компоненты</a> - это такие компоненты, которые используются на различных экранах без дополнительной модификации.
